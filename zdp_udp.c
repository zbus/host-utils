/*  Copyright(C) 2007 Christian Dietrich <stettberger@dokucode.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>

#include "zdp.h"

uint16_t crcUpdate(uint16_t crc, uint8_t data)
{
        uint16_t tmp;
        uint8_t j;

	tmp = data << 8;
        for (j = 0; j < 8; j ++)
	{
                if((crc ^ tmp) & 0x8000)
			crc = (crc << 1) ^ 0x1021;
                else
			crc = crc << 1;
                tmp = tmp << 1;
        }
	return crc;
}

uint16_t 
zdp_calc_crc(struct zdp_message *message, uint8_t datalen)
{
  uint16_t crc = 0, len;
  
  crc = crcUpdate(crc, message->dst);
  crc = crcUpdate(crc, message->src);
  crc = crcUpdate(crc, message->port);
  crc = crcUpdate(crc, message->flags);
  for ( len = 0; len < datalen; len++ ) {
    crc = crcUpdate(crc, message->data[len]);
  }
  crc = crcUpdate(crc, datalen);
  return crc;
}

int  
open_udp_socket(int port)
{
  struct sockaddr_in addr;

  int sock  = socket(AF_INET, SOCK_DGRAM, 0);
  if (sock < 0) error("socket");

  /* Make the socket reusable */
  int x;
  if(setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &x, sizeof (x)) == -1)
    error("setsockopt (SO_REUSEADDR)");

  /* Bind to an local port */
  if (port) {
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    if (bind(sock, (struct sockaddr *)&addr, sizeof(addr)) == -1)
      error("bind");
  }

  /* Make the socket non blocking */
  fcntl(sock, F_SETFL, O_NONBLOCK);

  return sock;
}

struct sockaddr_in*
get_sockaddr_in(const char *hostname, int port)
{
  struct hostent *hp;
  struct sockaddr_in *addr = malloc(sizeof(struct sockaddr_in));
  if (!addr) error("malloc");

  addr->sin_family = AF_INET;

  /* Get hostname */
  hp = gethostbyname(hostname);
  if (hp == 0) error("Unknown host");

  memcpy(&addr->sin_addr, hp->h_addr, hp->h_length);

  addr->sin_port = htons(port);

  return addr;
}

void
send_zdp_message(int sock, struct sockaddr_in* target, 
                 struct zdp_message *msg, int datalen) 
{
  int n = sendto(sock, msg, datalen + 6, 0, (struct sockaddr *)target, 
                 sizeof(struct sockaddr_in));
  if (n < 0) 
    error("sendto");
}

int
recv_zdp_message(int sock, int timeout, struct zdp_message **message, int *len)
{
  struct zdp_message *msg = malloc(sizeof(struct zdp_message));
  char *recv_buffer = malloc(sizeof(struct zdp_message) + 1);
  *message = msg;
  *len = 0;
  if (! msg || ! recv_buffer) 
    error("malloc");

  int useconds = 0;
  struct sockaddr_in remote;
  int remote_len = sizeof(remote);

  while (useconds < timeout) {
    char status;
    int n = recvfrom(sock, (char *)recv_buffer, sizeof(struct zdp_message), 0, 
                     (struct sockaddr *)&remote, &remote_len);
    if (n > 1) {
      *len = n - 7;
      memcpy(msg, recv_buffer + 1, sizeof(struct zdp_message));
      if(*recv_buffer == 'O') {
        free(recv_buffer);
        return 1;
      } else {
        free(recv_buffer);
        return 0;
      }
    }
    usleep(5000);
    useconds += 5;
  }
  return 0;

}

void
usage() 
{
  fprintf(stderr, "Options (zbus):\n"
          " -H <host>  -  host to connect to\n"
          " -P <port>  -  remote port to connect to\n"
          " -L <port>  -  local to send from\n\n"
          "Options (zdp), everythin in hex, like \"0a\":\n"
          " -s <src>   -  src address\n"
          " -d <dst>   -  dst address\n"
          " -p <port>  -  zdp port\n"
          " -f <flags> -  zdp flags\n\n"
          "All following data will be sent as message body\n"
          );
}


int 
main(int argc, char *argv[]) 
{
  int i, len = 0;
  struct zdp_message msg;
  char *host = NULL;
  int rport = 23514;
  int lport = 1200;
  memset(&msg, 0, sizeof(msg));
  for (i = 1; i < argc; i++) {
    if (argv[i][0] == '-') {

      if (argv[i][1] == 'P') 
        rport = strtol(argv[i+1], NULL, 10);
      if (argv[i][1] == 'H') 
        host = argv[i+1];
      if (argv[i][1] == 'L') 
        lport = strtol(argv[i+1], NULL, 10);
      
      if (argv[i][1] == 'h') {
        usage();
        exit(EXIT_SUCCESS);
      }
      
      if (argv[i][1] == 's') 
        msg.src = strtol(argv[i+1], NULL, 16);
      if (argv[i][1] == 'd') 
        msg.dst = strtol(argv[i+1], NULL, 16);
      if (argv[i][1] == 'p') 
        msg.port = strtol(argv[i+1], NULL, 16);
      if (argv[i][1] == 'f') 
        msg.flags = strtol(argv[i+1], NULL, 16);
      
      i++;
    } else {
      char *p = argv[i];
      while(*p) {
        msg.data[len] = *p;
        p++;
        len++;
      }
    }
  }

  msg.crc = htons(zdp_calc_crc(&msg, len));

  /* test remote options */
  if (!host || !rport || !lport) {
    usage();
    exit(EXIT_FAILURE);
  }


  int fd;
  fd = open_udp_socket(lport);
  struct sockaddr_in *addr = get_sockaddr_in(host, rport);

  send_zdp_message(fd, addr, &msg, len);

  /* answer */
  struct zdp_message *answer;
  if (recv_zdp_message(fd, 500, &answer, &len)) {
    if (! (zdp_calc_crc(answer, len) == ntohs(answer->crc))) {
      fprintf(stderr, "zdp: crc error\n");
      exit(EXIT_FAILURE);
    }
    char *res = (char *) answer;
    for (i = 0; i < len + 6; i++)
      putchar(res[i]);
  } else {
    if (len == 0) {
      fprintf(stderr, "zbus: no answer\n");
      exit(EXIT_FAILURE);
    }
    /* communication error */
    fprintf(stderr, "zbus error: ");
    char *res = (char *) answer;
    for (i = 0; i < len + 6; i++)
      putc(res[i], stderr);
    putc('\n', stderr);
    exit(EXIT_FAILURE);
  }
  close(fd);
  free(addr);
  free(answer);

  return 0;
}



