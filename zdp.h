#ifndef _ZBUS_H
#define _ZBUS_H

typedef unsigned char uint8_t;
typedef unsigned short int uint16_t;

struct zdp_message {
  uint8_t dst;
  uint8_t src;
  uint8_t port;
  uint8_t flags;

  uint16_t crc;
  char data[255];
};

void error(char *msg)
{
  perror(msg);
  exit(EXIT_FAILURE);
}

#endif
