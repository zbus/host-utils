 /*  Copyright(C) 2007 Christian Dietrich <stettberger@dokucode.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "zdp.h"
  
float aT = -167.123;
float bT = 0.275501;
float cT = -0.000102316;
float dT  = 1.92025e-08;

void 
usage() 
{
  fprintf(stderr, "reads an zdp message from stdin and decodes the kty adc values");
}

int main(int argc, char *argv[]) {
  if (argc > 1) {
    usage();
    exit(EXIT_SUCCESS);
  }
  int sensor = 0;
  struct zdp_message msg;
  int len = fread(&msg, 1, sizeof(struct zdp_message), stdin) - 6;
  uint8_t *p = msg.data;
  while ((char *)p < (msg.data + len)) {

    uint16_t adc = p[1] + p[0] * 256;

    if (p[1] != 0xff && p[0] != 0x3f) {
      float volt = (2.5/1023) * adc;
      float R = 2200/(5 - volt)*volt;
      float temperatur = aT + R*( bT + R*( cT + R*dT));
      printf("Sensor: %d C: %3.1f\n", sensor, temperatur);
    }
    p += 3;
    sensor++;
  }
  return 0;


}
